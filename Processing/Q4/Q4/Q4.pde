void setup()
{
  size(1020, 720, P3D);
  camera(0,0, -(height/2), tan(PI*30.0/180.0),0,0,0,0,-1);
}

   float R;
   float G;
   float B;
   float tR = 100;
   float tG = 255;
   float tB = 200;

Walker perlinWalker = new Walker();

void draw()
{
      R = map(noise(tR), 0 , 1 , 0, 255);
      G = map(noise(tG), 0 , 1 , 0, 255);
      B = map(noise(tB), 0 , 1 , 0, 255);
      
       noStroke();
       fill(R,G,B);
  
      tR += 0.1f;
      tG += 0.1f;
      tB += 0.1f;
 
  
   perlinWalker.render();
   perlinWalker.perlinWalk();
  
}
