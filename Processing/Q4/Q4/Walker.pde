public class Walker
{
 public float x;
 public float y;
 public float tx = 0;
 public float ty = 10000;
 public float size;
 public float tSize = 0;
 
 void render()
 {
  circle(x,y,size); 
 }
  
  void perlinWalk()
  {
    x = map(noise(tx), 0 , 1 , -640, 640);
    y = map(noise(ty), 0, 1, -360, 360);
    size = map(noise(tSize), 0, 1, 5, 150);
    
    tx += 0.01f;
    ty += 0.01f;
    tSize += 0.01f;
    
  }
}
