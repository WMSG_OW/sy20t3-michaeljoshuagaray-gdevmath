Walker[] myWalkers = new Walker[8];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
    
  for (int i = 0; i < 8; i++)
  {
    myWalkers[i] = new Walker(Window.left + 50, 50);
    myWalkers[i].mass = i + random(1,5);
    myWalkers[i].position.y = i * 50;
    myWalkers[i].setColor(random(1, 255),random(1, 255),random(1, 255), 255);
  }
}

PVector wind = new PVector(0.1f, 0);


void draw()
{
  background(255);
  
  noStroke();
  for (Walker my : myWalkers)
  {
    my.applyForce(wind);
    my.render();
    my.update();
  
    if (my.position.x >= 0)
    {
      my.applyFriction();
      if (my.velocity.x <= 0)
      {
        my.acceleration.x *= 0;
        my.velocity.x *= 0;
      }
    }
  }
}
