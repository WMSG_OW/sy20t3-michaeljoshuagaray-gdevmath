Walker myWalker = new Walker();
Walker myWalker2 = new Walker();
Walker[] myWalkers = new Walker[10];

void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
   /*myWalker.position = new PVector();
   myWalker.mass = 20;
   myWalker.scale = myWalker.mass * 10;
   
   myWalker2.position = new PVector(150,150);
   myWalker2.mass = 15;
   myWalker2.scale = myWalker2.mass * 10;*/
  
  //int position = 0;
  
  for (int i = 0; i < 10; i++)
  {
    myWalkers[i] = new Walker();
    myWalkers[i].position = new PVector(random(500), random(100));
    myWalkers[i].mass = random(5,20);
    myWalkers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
    myWalkers[i].scale = myWalkers[i].mass * 10;
  }
  
}
  void draw()
{
  background(255);
  
    
    for(int i = 0; i < 10; i++)
    {
      noStroke();
      myWalkers[i].update();
      myWalkers[i].render();
     for(int j = 0; j < 9; j++)
     {
       if(i!=j);
       {
         myWalkers[i].applyForce(myWalkers[j].calculateAttraction(myWalkers[i]));
       }
     }
      
    }
    

  
  /*myWalker.update();
  myWalker.render();
  
  myWalker2.update();
  myWalker2.render();
  
  myWalker2.applyForce(myWalker.calculateAttraction(myWalker2));
  myWalker.applyForce(myWalker2.calculateAttraction(myWalker));*/
}
