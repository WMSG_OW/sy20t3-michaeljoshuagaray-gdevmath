void setup()
{
  size(1020, 720, P3D);
  camera(0,0, -(height/2), tan(PI*30.0/180.0),0,0,0,0,-1);
}

void draw()
{
  float gaussian = randomGaussian();
  
  float sd = 400;
  float mean = 0;
  float x = sd * gaussian + mean;
  float y = random(0,400);
  int R = int(random(255));
 int G = int(random(255));
 int B = int(random(255));
 int A = int(random(10,100));
 
 float standardDevscale = 200;
 float meanScale = 40;
 
 float scale = standardDevscale * gaussian + meanScale;
 
 
  
  noStroke();
  fill(R,G,B,A);
  circle(x,y,scale);
}
