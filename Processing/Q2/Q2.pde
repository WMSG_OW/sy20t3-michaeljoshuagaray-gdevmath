void setup()
{
  size(1020, 720, P3D);
  camera(0,0, -(height/2), tan(PI*30.0/180.0),0,0,0,0,-1);
}

Walker myWalker = new Walker();
Walker xWalker = new Walker();
Walker bWalker = new Walker();



void draw()
{
  
  int R = int(random(255));
 int G = int(random(255));
 int B = int(random(255));
 int A = int(random(50,100));
  
  
  fill(R,G,B,A);
  bWalker.randomWalkBiased();
  bWalker.render();
  
}
