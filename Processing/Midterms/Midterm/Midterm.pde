void setup()
{
  size(1920,1080,P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  circles = new Walker[100];
  noStroke();
  MovingWalkers();
}

Walker circles[];
Walker blackhole;

void MovingWalkers()
{
   int numCircles = 100;
   for (int i = 0; i < numCircles; i++)
   {
      float scale = random(5, 30);
      float value = randomGaussian();
      float standardDeviation = 100;
      float Horizontal = random(Window.left, Window.right);
      float Vertical = random(Window.bottom, Window.top);
      float x = standardDeviation * value + Horizontal;
      float y = standardDeviation * value + Vertical;
      circles[i] = new Walker(x, y, scale); 
      circles[i].r = map(noise(random(255)), 0, 1, 0, 255);
      circles[i].g = map(noise(random(255)), 0, 1, 0, 255);
      circles[i].b = map(noise(random(255)), 0, 1, 0, 255);
      circles[i].a = map(noise(random(255)), 0, 1, 0, 255);
      blackhole = new Walker(random(Window.left, Window.right), random(Window.bottom, Window.top), 50);
   }
}

Walker mover = new Walker();
int frame = 0;
void draw()
{
  frame++;
  background(0);
  if (frame >= 220)
  {
   frame = 0;
   MovingWalkers();
  }

  for (Walker my : circles)
  {
    my.render();
    PVector direction = PVector.sub(blackhole.position, my.position);
    direction.normalize().mult(5);
    my.position.add(direction);
  }
  blackhole.render();
}
