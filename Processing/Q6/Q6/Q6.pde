void setup ()
{
 size(1080,720, P3D );
 camera(0,0,Window.eyeZ, 0, 0, 0, 0, -1, 0);
}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth/2;
  float y = -(mouseY - Window.windowHeight/2);
  
  return new PVector(x,y);
}

void draw()
{
 background(255);
 
 strokeWeight(10);
 stroke(255,0,0);
 
 PVector mouse = mousePos();
 //mouse.mult(2);
 mouse.normalize().mult(500);
 //1st side
 line(0,0,mouse.x,mouse.y);
 
 //other side 
 line(0,0,-mouse.x,-mouse.y);
 
 
 strokeWeight(5);
 stroke(255,255,255);
 
 //1st innerside
 line(0,0,mouse.x,mouse.y);
 
 //other innerside 
 line(0,0,-mouse.x,-mouse.y);
 
 
 //handle 
 strokeWeight(7);
 stroke(0,0,0);
 line(0,0,mouse.x/4,mouse.y/4);
 
 
 
 
 println(mouse.mag());
}
