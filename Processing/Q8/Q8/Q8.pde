Walker myWalker;
Walker[] myWalkers = new Walker[8];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  myWalker = new Walker();
  myWalker.position.x = Window.left + 50;
  myWalker.mass = 10;
  //mover.acceleration = new PVector(0.1, 0);
  
  for (int i = 0; i < 8; i++)
  {
    myWalkers[i] = new Walker(Window.left + 50, 200);
    myWalkers[i].mass = i + 1;
    myWalkers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
  }
}

PVector wind = new PVector(0.1f, 0);
PVector gravity = new PVector(0, -1f);

void draw()
{
  background(255);
  
  noStroke();
  for (Walker m : myWalkers)
  {
    m.render();
    m.update();
    m.applyForce(wind);
    m.applyForce(gravity);
    
    if (m.position.x > Window.right)
    {
      m.velocity.x *= -1;
      m.position.x = Window.right;
    }
    
    if (m.position.y < Window.bottom)
    {
      m.velocity.y *= -1; 
      m.position.y = Window.bottom;
    }
  }
  

}
