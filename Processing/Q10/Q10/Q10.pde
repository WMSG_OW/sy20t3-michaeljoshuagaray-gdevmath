Walker myWalker = new Walker();
Walker[] myWalkers = new Walker[10];

Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);


void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  //myWalker.setColor(155, 105, 50, 255);
  //myWalker.mass = 5;
  
  int position = 0;
  
  for (int i = 0; i < 10; i++)
  {
    myWalkers[i] = new Walker(Window.left + position, 200);
    myWalkers[i].mass = i + 1;
    myWalkers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
    position += 100;
  }
  
}

PVector wind = new PVector(0.1f, 0);

void draw()
{
  background(255);
  
  ocean.render();
  noStroke();
  
   for (Walker my : myWalkers)
  {
    my.applyGravity();
    my.render();
    my.update();
    my.applyForce(wind);
    my.applyFriction();
    
    if (my.position.x > Window.right)
    {
      my.velocity.x *= -1;
      my.position.x = Window.right;
    }
    
    if (my.position.y < Window.bottom)
    {
      my.velocity.y *= -1; 
      my.position.y = Window.bottom;
    }
    
    
    if (ocean.isCollidingWith(my))
    {
      my.applyForce(ocean.calculateDragForce(my)); 
    }
    
  }
}
